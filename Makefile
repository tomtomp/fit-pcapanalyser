# Template found on http://stackoverflow.com/questions/2481269/how-to-make-a-simple-c-makefile
CXX=g++
RM=rm -f
CPPFLAGS=-std=c++11 -O3
LDFLAGS=

SOURCE_DIR="src/"

SRCS=   ${SOURCE_DIR}/main.cpp \
        ${SOURCE_DIR}/Settings.cpp \
        ${SOURCE_DIR}/AnalyserApp.cpp \
        ${SOURCE_DIR}/Filter.cpp \
        ${SOURCE_DIR}/NetAddress.cpp \
        ${SOURCE_DIR}/PGH.cpp \
        ${SOURCE_DIR}/PPH.cpp \
        ${SOURCE_DIR}/PacketAnalyser.cpp \
        ${SOURCE_DIR}/EthernetHeader.cpp \
        ${SOURCE_DIR}/Ipv6Header.cpp \
        ${SOURCE_DIR}/Ipv4Header.cpp \
        ${SOURCE_DIR}/TcpHeader.cpp \
        ${SOURCE_DIR}/UdpHeader.cpp \
        ${SOURCE_DIR}/ArpHeader.cpp

OBJS=$(subst .cpp,.o,$(SRCS))

all: analyzer

analyzer: $(OBJS)
	$(CXX) $(LDFLAGS) -o analyzer $(OBJS)

depend: .depend

.depend: $(SRCS)
	rm -f ./.depend
	$(CXX) $(CPPFLAGS) -MM $^>>./.depend;

clean:
	$(RM) $(OBJS)

dist-clean: clean
	$(RM) *~ .depend

include .depend
