/**
 * @file EthernetHeader.cpp
 * @author Tomas Polasek
 * @brief Class for containing the Ethernet header info.
 */

#include "EthernetHeader.h"

namespace pa
{
    EthernetHeader::EthernetHeader(const uint8_t *headerStart, uint64_t remainingSize) :
        BaseNetworkHeader(sizeof(EthernetHeaderForm), FilterT::ETH)
    {
        if (remainingSize < mSize)
        {
            throw std::runtime_error("Unable to parse Ethernet header, not enough data!");
        }

        //std::memcpy(&mEthernetFrame, reinterpret_cast<const EthernetHeaderForm*>(headerStart), sizeof(EthernetHeaderForm));
        mEthernetHeader = reinterpret_cast<const EthernetHeaderForm*>(headerStart);

        const uint16_t *typePtr{&(mEthernetHeader->type)};
        uint16_t type{ntohs(*typePtr)};

        if (type == ETH_DOT1Q_DOUBLE)
        { // There should be 2 DOT1Q headers.
            // Increase the size of the Ethernet header.
            mSize += ETH_DOT1Q_SIZE;
            if (remainingSize < mSize)
            {
                throw std::runtime_error("Unable to parse Ethernet header, not enough data (DOT1Q_DOUBLE)!");
            }

            // Move to the next type.
            typePtr += ETH_DOT1Q_SIZE_UINT16;
            type = ntohs(*typePtr);
            // We do not check, if there is a second DOT1Q header.
        }

        if (type == ETH_DOT1Q_SINGLE)
        { // One DOT1Q header from the typePtr.
            // Increase the size of the Ethernet header.
            mSize += ETH_DOT1Q_SIZE;
            if (remainingSize < mSize)
            {
                throw std::runtime_error("Unable to parse Ethernet header, not enough data (DOT1Q_SINGLE)!");
            }

            // Move to the next type.
            typePtr += ETH_DOT1Q_SIZE_UINT16;
            type = ntohs(*typePtr);
        }

        switch (type)
        {
            case NET_TYPE_IPV4:
            {
                mNextLayerType = FilterT::IPV4;
                break;
            }
            case NET_TYPE_IPV6:
            {
                mNextLayerType = FilterT::IPV6;
                break;
            }
            case NET_TYPE_ARP:
            {
                mNextLayerType = FilterT::ARP;
                break;
            }
            default:
            {
                mNextLayerType = FilterT::UNK;
                break;
            }
        }

        mSrcAddress.setAddr(mEthernetHeader->srcMac, FilterT::ETH);
        mDestAddress.setAddr(mEthernetHeader->destMac, FilterT::ETH);
    }

    void EthernetHeader::printContent() const
    {
        std::cout << "Content of the Ethernet header : \n" <<
                  "\tsrcMac \t\t: ";

        std::cout << std::hex;
        for (uint8_t iii = 0; iii < 6; ++iii)
        {
            std::cout << static_cast<uint16_t>(mEthernetHeader->srcMac[iii]) << (iii == 5 ? "\n" : ":");
        }
        std::cout << std::dec;

        std::cout << "\tdestMac \t: ";

        std::cout << std::hex;
        for (uint8_t iii = 0; iii < 6; ++iii)
        {
            std::cout << static_cast<uint16_t>(mEthernetHeader->destMac[iii]) << (iii == 5 ? "\n" : ":");
        }
        std::cout << std::dec;

        std::cout << "\ttype \t\t: " << std::hex << ntohs(mEthernetHeader->type) << std::dec << " (";
        switch (mNextLayerType)
        {
            case FilterT::IPV4:
            {
                std::cout << "IPv4";
                break;
            }
            case FilterT::IPV6:
            {
                std::cout << "IPv6";
                break;
            }
            default:
            {
                std::cout << "UNK";
                break;
            }
        }
        std::cout << ")" << std::endl;
    }
}
