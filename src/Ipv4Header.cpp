/**
 * @file Ipv4Header.cpp
 * @author Tomas Polasek
 * @brief Class for containing information about IPv4 header.
 */

#include "Ipv4Header.h"

namespace pa
{
    Ipv4Header::Ipv4Header(const uint8_t *headerStart, uint64_t remainingSize) :
        BaseNetworkHeader(sizeof(Ipv4HeaderForm), FilterT::IPV4)
    {
        if (remainingSize < mSize)
        {
            throw std::runtime_error("Unable to parse IPv4 header, not enough data!");
        }

        mIpv4Header = reinterpret_cast<const Ipv4HeaderForm*>(headerStart);

        // Calculate the correct size.
        mSize = static_cast<uint64_t>(mIpv4Header->versionIhl & 0x0F) * IHL_MULTIPLIER;

        if (mSize < IHL_MIN * IHL_MULTIPLIER)
        {
            throw std::runtime_error("Unable to parse IPv4 header, IHL < 5!");
        }

        if (remainingSize < mSize)
        {
            throw std::runtime_error("Unable to parse IPv4 header, not enough data!");
        }

        switch (static_cast<NetNextHeaderType>(mIpv4Header->protocol))
        {
            case NetNextHeaderType::TCP:
            {
                mNextLayerType = FilterT::TCP;
                break;
            }
            case NetNextHeaderType::UDP:
            {
                mNextLayerType = FilterT::UDP;
                break;
            }
            default:
            {
                mNextLayerType = FilterT::UNK;
                break;
            }
        }

        mSrcAddress.setAddr(mIpv4Header->srcIp, FilterT::IPV4);
        mDestAddress.setAddr(mIpv4Header->destIp, FilterT::IPV4);
    }

    void Ipv4Header::printContent() const
    {
        std::cout << "Content of the IPv4 header : \n" <<
                  "\ttotalLength \t: " << ntohs(mIpv4Header->totalLength) << "\n" <<
                  "\tidentification \t: " << ntohs(mIpv4Header->identification) << "\n" <<
                  "\tttl \t\t: " << static_cast<uint16_t>(mIpv4Header->ttl) << "\n" <<
                  "\tprotocol \t: " << static_cast<uint16_t>(mIpv4Header->protocol);
        switch (mNextLayerType)
        {
            case FilterT::TCP:
            {
                std::cout << " (TCP)\n";
                break;
            }
            case FilterT::UDP:
            {
                std::cout << " (UDP)\n";
                break;
            }
            default:
            {
                std::cout << " (UNK)\n";
                break;
            }
        }

        std::cout << "\tsrcIp \t\t: ";
        for (uint8_t iii = 0; iii < 4; ++iii)
        {
            std::cout << static_cast<uint16_t>(mIpv4Header->srcIp[iii]) << ((iii < 3) ? '.' : '\n');
        }
        std::cout << "\tdestIp \t\t: ";
        for (uint8_t iii = 0; iii < 4; ++iii)
        {
            std::cout << static_cast<uint16_t>(mIpv4Header->destIp[iii]) << ((iii < 3) ? '.' : '\0');
        }

        std::cout << std::endl;
    }

    uint64_t Ipv4Header::getTotalSize() const
    {
        return ntohs(mIpv4Header->totalLength);
    }
}
