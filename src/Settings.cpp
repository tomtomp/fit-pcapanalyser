/**
 * @file Settings.cpp
 * @author Tomas Polasek
 * @brief Class used for parsing arguments passed by the user.
 */

#include "Settings.h"

extern "C"
{
#   include <unistd.h>
}

namespace pa
{
    const Settings::TranslationRecord Settings::FILTER_TYPE_TRANSLATE[] =
    {
        TranslationRecord("mac", FilterT::ETH),
        TranslationRecord("ipv4", FilterT::IPV4),
        TranslationRecord("ipv6", FilterT::IPV6),
        TranslationRecord("tcp", FilterT::TCP),
        TranslationRecord("udp", FilterT::UDP)
    };

    Settings::Settings(int argc, char *argv[])
    {
        if (!parseArguments(argc, argv))
        {
            printUsage();

            throw std::runtime_error("Unable to parse command line arguments.");
        }
    }

    bool Settings::parseArguments(int argc, char *argv[])
    {
        if (argc < MIN_NUM_ARGS)
        {
            std::cerr << "Not enough parameters!" << std::endl;
            return false;
        }

        int c{0};
        const char *filterTypes{nullptr};
        const char *filterValues{nullptr};

        while ((c = getopt (argc, argv, "i:f:v:sdt")) != -1)
        {
            switch (c)
            {
                case 'i':
                {
                    if (!mInputFile.empty())
                    {
                        std::cerr << "Input filename specified multiple times" << std::endl;
                        return false;
                    }

                    mInputFile = optarg;
                    break;
                }
                case 'f':
                {
                    if (filterTypes != nullptr)
                    { // Filter type has been specified more than once.
                        std::cerr << "Filter type specified multiple times" << std::endl;
                        return false;
                    }

                    /*
                    // We are parsing type of the filter.
                    FilterT val = translateFilterType(optarg);

                    if (val == FilterT::UNK)
                    { // Unknown filter specified.
                        std::cerr << "Unknown filter type specified!" << std::endl;
                        return false;
                    }
                     */

                    filterTypes = optarg;
                    break;
                }
                case 'v':
                {
                    if (filterValues != nullptr)
                    { // Filter value has been specified more than once.
                        std::cerr << "Filter value specified multiple times" << std::endl;
                        return false;
                    }
                    // Get the value for later parsing.
                    filterValues = optarg;
                    break;
                }
                case 's':
                {
                    if (mFilter.sourceFilter())
                    { // s flag specified multiple times.
                        std::cerr << "Source flag specified multiple times!" << std::endl;
                        return false;
                    }

                    mFilter.setSourceFilter();

                    break;
                }
                case 'd':
                {
                    if (mFilter.destinationFilter())
                    { // d flag specified multiple times.
                        std::cerr << "Destination flag specified multiple times!" << std::endl;
                        return false;
                    }

                    mFilter.setDestinationFilter();

                    break;
                }
                case 't':
                {
                    mVerbose = true;
                    break;
                }
                default:
                {
                    std::cerr << "Unknown option : " << c << std::endl;
                    return false;
                }
            }
        }

        if (filterTypes == nullptr)
        {
            std::cerr << "Filter type is a required parameter!" << std::endl;
            return false;
        }

        if (filterValues == nullptr)
        {
            std::cerr << "Filter value is a required parameter!" << std::endl;
            return false;
        }

        if (!(mFilter.sourceFilter() || mFilter.destinationFilter()))
        {
            std::cerr << "At least one of the -s|-d has to be specified!" << std::endl;
            return false;
        }

        if (optind < argc)
        { // Too many arguments.
            std::cerr << "Too many arguments specified!" << std::endl;
            return false;
        }

        // Parse the filter value.
        return parseFilterTypesValues(filterTypes, filterValues);
    }

    void Settings::printUsage() const
    {
        std::cout << "Usage : \n" <<
                     "./analyzer -f filterType -v filterValue -s|-d -i inputFile\n" <<
                     "\t-i inputFile - optional argument, if not specified stdin is used instead\n" <<
                     "\t-f filterType - required argument, available values : mac, ipv4, ipv6, tcp, udp\n" <<
                     "\t\tMultiple values can be specified using \";\" as a divider.\n" <<
                     "\t-v filterValue - required argument, available values depend of filterType.\n" <<
                     "\t\tMultiple values can be specified using \",\" as a divider and \";\" as a \n" <<
                     "\t\tvalue type divider. Value examples : \n"<<
                     "\t\tmac - e.g. 5C:D5:96:2C:38:63\n" <<
                     "\t\tipv4 - e.g. 192.168.1.100\n" <<
                     "\t\tipv6 - e.g. 2001::1\n" <<
                     "\t\ttcp/udp - e.g. 80\n" <<
                     "\t\tany - top10\n" <<
                     "\t-s or -d  - only one can (and must) be specified, the filter is applied to the \n" <<
                     "\t\tsource (if -s is specified), or to the destination (if -d is specified).\n" <<
                     "\t-t (talky) - set the application to verbose mode." <<
                     std::endl;
    }

    FilterT Settings::translateFilterType(const char *str)
    {
        for (auto &rec : FILTER_TYPE_TRANSLATE)
        {
            if (std::strcmp(str, rec.str) == 0)
            {
                return rec.val;
            }
        }

        return FilterT::UNK;
    }

    bool Settings::parseFilterTypesValues(const char *types, const char *values)
    {
        std::stringstream ssTypes(types);
        std::vector<std::string> vecTypes;
        std::stringstream ssValues(values);
        std::vector<std::string> vecValues;

        std::string element;

        while (std::getline(ssTypes, element, ';'))
        {
            vecTypes.emplace_back(std::move(element));
        }

        while (std::getline(ssValues, element, ';'))
        {
            vecValues.emplace_back(std::move(element));
        }

        if (vecTypes.size() != vecValues.size())
        {
            std::cerr << "Number of types does not correspond to number of value types!" << std::endl;
            return false;
        }

        for (uint64_t iii = 0; iii < vecTypes.size(); ++iii)
        { // Go through all types.
            const std::string &type{vecTypes[iii]};
            std::stringstream valueList{vecValues[iii]};

            while (std::getline(valueList, element, ','))
            { // Go through all values for each type.
                if (!strParseFilterTypeValue(type, element))
                {
                    return false;
                }
            }
        }

        return true;
    }

    bool Settings::strParseFilterTypeValue(const std::string &type, const std::string &value)
    {
        FilterT filterType{FilterT::UNK};

        for (const auto &r : FILTER_TYPE_TRANSLATE)
        {
            if (type == r.str)
            {
                filterType = r.val;
                break;
            }
        }

        if (filterType == FilterT::UNK)
        {
            throw std::runtime_error("Error, unknown filter type!");
        }

        if (value == "top10")
        {
            return mFilter.setTop10Filter(filterType);
        }

        // Storage for the parsed value.
        uint8_t storage[MAX_FILTER_BYTES] = {0, };

        switch (filterType)
        {
            case FilterT::ETH:
            {
                std::stringstream ssMac(value);

                char colon;
                int val;
                for (uint64_t iii = 0; iii < ETH_ADDRESS_SIZE; ++iii)
                { // For each byte in the MAC address.
                    ssMac >> std::hex >> val;
                    if (!ssMac)
                    {
                        std::cerr << "Failed in parsing of MAC address"  << std::endl;
                        return false;
                    }
                    ssMac >> colon;

                    storage[iii] = static_cast<uint8_t>(val);
                }

                mFilter.addFilter(filterType, NetAddress(storage, FilterT::ETH));

                break;
            }
            case FilterT::IPV4:
            {
                std::stringstream ssIpv4(value);

                char dot;
                int val;
                for (uint64_t iii = 0; iii < IPV4_ADDRESS_SIZE; ++iii)
                { // For each byte in the MAC address.
                    ssIpv4 >> val;
                    if (!ssIpv4)
                    {
                        std::cerr << "Failed in parsing of IPv4 address"  << std::endl;
                        return false;
                    }
                    ssIpv4 >> dot;

                    storage[iii] = static_cast<uint8_t>(val);
                }

                mFilter.addFilter(filterType, NetAddress(storage, FilterT::IPV4));

                break;
            }
            case FilterT::IPV6:
            {
                if (inet_pton(AF_INET6, value.c_str(), storage) != 1)
                {
                    std::cerr << "Failed in parsing of IPv6 address" << std::endl;
                    return false;
                }

                mFilter.addFilter(filterType, NetAddress(storage, FilterT::IPV6));

                break;
            }
            case FilterT::TCP:
            {
                std::stringstream ssTCP(value);

                ssTCP >> *reinterpret_cast<uint16_t*>(&storage[0]);

                // Convert to correct endian.
                *reinterpret_cast<uint16_t*>(&storage[0]) = htons(*reinterpret_cast<uint16_t*>(&storage[0]));

                mFilter.addFilter(filterType, NetAddress(storage, FilterT::TCP));

                break;
            }
            case FilterT::UDP:
            {
                std::stringstream ssTCP(value);

                ssTCP >> *reinterpret_cast<uint16_t*>(&storage[0]);

                // Convert to correct endian.
                *reinterpret_cast<uint16_t*>(&storage[0]) = htons(*reinterpret_cast<uint16_t*>(&storage[0]));

                mFilter.addFilter(filterType, NetAddress(storage, FilterT::UDP));

                break;
            }
            default:
            {
                std::cerr << "Unknown filter type specified!" << std::endl;
                return false;
            }
        }

        return true;
    }
}
