/**
 * @file Filter.cpp
 * @author Tomas Polasek
 * @brief Filter class possibly containing multiple sub-filters.
 */

#include "Filter.h"

namespace pa
{
    Filter::Filter()
    { }

    void Filter::addFilter(FilterT type, const NetAddress &address)
    {
        switch (type)
        {
            case FilterT::ETH:
            {
                mRules[LAYER_LINK].emplace_back(type, address);

                mTopmostLayer = mTopmostLayer < LAYER_LINK ? LAYER_LINK : mTopmostLayer;

                break;
            }
            case FilterT::IPV4:
            case FilterT::IPV6:
            {
                mRules[LAYER_NETWORK].emplace_back(type, address);

                mTopmostLayer = mTopmostLayer < LAYER_NETWORK ? LAYER_NETWORK : mTopmostLayer;

                break;
            }
            case FilterT::TCP:
            case FilterT::UDP:
            {
                mRules[LAYER_TRANSPORT].emplace_back(type, address);

                mTopmostLayer = mTopmostLayer < LAYER_TRANSPORT ? LAYER_TRANSPORT : mTopmostLayer;

                break;
            }
            default:
            {
                // Filter for unknown layer, no action taken.
            }
        }
    }

    bool Filter::setTop10Filter(FilterT type)
    {
        /*
        if (!mRules[LAYER_LINK].empty() ||
            !mRules[LAYER_NETWORK].empty() ||
            !mRules[LAYER_TRANSPORT].empty())
            */
        if (mTop10Mode)
        { // More than one top10 specified.
            return false;
        }

        mTop10Type = type;
        mTop10Mode = true;

        LayerT layer{layerFromFilter(type)};
        mTopmostLayer = mTopmostLayer < layer ? layer : mTopmostLayer;

        return true;
    }

    bool Filter::match(const std::vector<std::unique_ptr<BaseNetworkHeader>> &headers) const
    {
        assert(mSourceFilter || mDestinationFilter);
        assert(headers.size() <= NUM_LAYERS);

        for (uint64_t iii = headers.size(); iii < NUM_LAYERS; ++iii)
        {
            if (mRules[iii].size() != 0)
            { // Not enough layers in given packet.
                return false;
            }
        }

        LayerT top10Layer{layerFromFilter(mTop10Type)};

        if (mTop10Mode && headers.size() <= top10Layer)
        { // Not enough layers in given packet.
            return false;
        }

        for (uint64_t iii = 0; iii < headers.size(); ++iii)
        {
            // Get the tested header.
            auto &header(headers[iii]);
            // Get the list of rules for tested header.
            auto &rules(mRules[iii]);

            // If there are no rules, default to satisfied.
            bool satisfied{rules.size() == 0};

            if (mTop10Mode && top10Layer == iii && header->layerType() != mTop10Type)
            { // Check that the protocol on top10 layer is the correct one.
                return false;
            }

            // Only one of the rules must be satisfied.
            for (auto &r : rules)
            {
                if (header->layerType() == r.type())
                {
                    if (mSourceFilter)
                    {
                        satisfied = r.compare(header->src());
                    }

                    if (mDestinationFilter && !satisfied)
                    {
                        satisfied = r.compare(header->dest());
                    }

                    if (satisfied)
                    { // Only one satisfied rule is needed per layer.
                        break;
                    }
                }
            }

            if (!satisfied)
            {
                return false;
            }
        }

        return true;
    }
}

