/**
 * @file PGH.cpp
 * @author Tomas Polasek
 * @brief Class used for interpreting binary data of global pcap header.
 */

#include "PGH.h"

namespace pa
{
    PGH::PGH(std::istream *inputStream)
    {
        char *readPtr{reinterpret_cast<char*>(&mHeader)};
        inputStream->read(readPtr, sizeof(PcapGlobalHeaderForm));
        if (!(*inputStream))
        {
            throw std::runtime_error("Unable to read whole global pcap header!");
        }

        switch (mHeader.magicNumber)
        {
            case MAGIC_NORMAL:
            {
                // No action needed.
                break;
            }
            case MAGIC_REVERSED:
            {
                // We need to swap nibbles.
                mSwap = true;
                break;
            }
            case MAGIC_NANO_NORMAL:
            {
                // usec -> nsec.
                mNanoS = true;
                break;
            }
            case MAGIC_NANO_REVERSED:
            {
                // usec -> nsec + nibble swapping.
                mSwap = true;
                mNanoS = true;
                break;
            }
            default:
            {
                std::cerr << "Unknown magic number in the pcap global header!" << std::endl;
                throw std::runtime_error("Unable to read whole global pcap header!");
            }
        }

        if (mSwap)
        {
            nibbleSwapper(mHeader.versionMajor);
            nibbleSwapper(mHeader.versionMinor);
            nibbleSwapper(mHeader.thiszone);
            nibbleSwapper(mHeader.sigfigs);
            nibbleSwapper(mHeader.snaplen);
            nibbleSwapper(mHeader.network);
        }

        if (mHeader.network != ETHERNET_LINKTYPE)
        {
            std::cerr << "This application can only read pcap files containing Ethernet frames!" << std::endl;
            throw std::runtime_error("Wrong LinkType!");
        }
    }

    void PGH::printContent() const
    {
        std::cout << "Content of pcap global header: \n" <<
                  "\tmagicNumber \t: " << std::hex << mHeader.magicNumber << std::dec << "\n" <<
                  "\tversion \t\t: " << mHeader.versionMajor << "."
                  << mHeader.versionMinor <<  "\n" <<
                  "\tthiszone \t\t: " << mHeader.thiszone << "\n" <<
                  "\tsigfigs \t\t: " << mHeader.sigfigs << "\n" <<
                  "\tsnaplen \t\t: " << mHeader.snaplen << "\n" <<
                  "\tnetwork \t\t: " << mHeader.network << " (ETH)"<< std::endl;
    }

}
