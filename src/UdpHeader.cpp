/**
 * @file UdpHeader.cpp
 * @author Tomas Polasek
 * @brief Class for containing information about UDP header.
 */

#include "UdpHeader.h"

namespace pa
{
    UdpHeader::UdpHeader(const uint8_t *headerStart, uint64_t remainingSize) :
        BaseNetworkHeader(sizeof(UdpHeaderForm), FilterT::UDP)
    {
        if (remainingSize < mSize)
        {
            throw std::runtime_error("Unable to parse TCP header, not enough data!");
        }

        mUdpHeader = reinterpret_cast<const UdpHeaderForm*>(headerStart);

        mSrcAddress.setAddr(mUdpHeader->srcPort, FilterT::UDP);
        mDestAddress.setAddr(mUdpHeader->destPort, FilterT::UDP);
    }

    void UdpHeader::printContent() const
    {
        std::cout << "Content of the Udp header : \n" <<
                  "\tsrcPort \t: " << ntohs(*reinterpret_cast<const uint16_t*>(mUdpHeader->srcPort)) << "\n" <<
                  "\tdestPort \t: " << ntohs(*reinterpret_cast<const uint16_t*>(mUdpHeader->destPort)) << "\n" <<
                  "\tlength \t\t: " << ntohs(mUdpHeader->length) << std::endl;
    }
}
