/**
 * @file PacketAnalyser.cpp
 * @author Tomas Polasek
 * @brief Used for analysing data contained within a packet.
 */

#include "PacketAnalyser.h"

namespace pa
{
    PacketAnalyser::PacketAnalyser()
    {
    }

    void PacketAnalyser::printContent() const
    {
        for (auto &h : mHeaders)
        {
            h->printContent();
        }
    }

    void PacketAnalyser::analyse(const std::vector<uint8_t> &data)
    {
        uint64_t totalSize{data.size()};
        uint64_t remainingSize{totalSize};

        uint64_t index{0};

        mHeaders.emplace_back(new EthernetHeader(&data[0], remainingSize));
        auto &ethernetHeader(mHeaders[index]);

        remainingSize -= ethernetHeader->headerSize();
        // TODO - + 4 bytes for the CRC?
        mHeaderSizes[LayerT::LAYER_LINK] = ethernetHeader->headerSize();
        index += ethernetHeader->headerSize();

        uint64_t padding{0};

        switch (ethernetHeader->nextLayerType())
        {
            case FilterT::IPV6:
            {
                Ipv6Header *header{new Ipv6Header(&data[index], remainingSize)};

                if (totalSize == MIN_FRAME_SIZE &&
                    header->getTotalSize() + mHeaderSizes[LayerT::LAYER_LINK] < MIN_FRAME_SIZE)
                { // The frame has been padded from the right.
                    padding = MIN_FRAME_SIZE - header->getTotalSize() - mHeaderSizes[LayerT::LAYER_LINK];
                    mHeaderSizes[LayerT::LAYER_LINK] += padding;
                }

                mHeaders.emplace_back(header);
                break;
            }
            case FilterT::IPV4:
            {
                Ipv4Header *header{new Ipv4Header(&data[index], remainingSize)};

                if (totalSize == MIN_FRAME_SIZE &&
                    header->getTotalSize() + mHeaderSizes[LayerT::LAYER_LINK] < MIN_FRAME_SIZE)
                { // The frame has been padded from the right.
                    padding = MIN_FRAME_SIZE - header->getTotalSize() - mHeaderSizes[LayerT::LAYER_LINK];
                    mHeaderSizes[LayerT::LAYER_LINK] += padding;
                }

                mHeaders.emplace_back(header);
                break;
            }
            case FilterT::ARP:
            {
                ArpHeader *header{new ArpHeader(&data[index], remainingSize)};

                if (totalSize == MIN_FRAME_SIZE &&
                    header->getTotalSize() + mHeaderSizes[LayerT::LAYER_LINK] < MIN_FRAME_SIZE)
                { // The frame has been padded from the right.
                    padding = MIN_FRAME_SIZE - header->getTotalSize() - mHeaderSizes[LayerT::LAYER_LINK];
                    mHeaderSizes[LayerT::LAYER_LINK] += padding;
                }

                mHeaders.emplace_back(header);
                break;
            }
            default:
            {
                // Unknown next header -> no work to be done.
                return;
            }
        }

        auto &networkHeader(mHeaders[1]);
        remainingSize -= networkHeader->headerSize();
        mHeaderSizes[LayerT::LAYER_NETWORK] = networkHeader->headerSize();
        index += networkHeader->headerSize();

        switch (networkHeader->nextLayerType())
        {
            case FilterT::TCP:
            {
                mHeaders.emplace_back(
                    new TcpHeader(
                        &data[index],
                        remainingSize));
                break;
            }
            case FilterT::UDP:
            {
                mHeaders.emplace_back(
                    new UdpHeader(
                        &data[index],
                        remainingSize));
                break;
            }
            default:
            {
                // Unknown next header -> no work to be done.
                return;
            }
        }

        auto &transportHeader(mHeaders[2]);
        remainingSize -= transportHeader->headerSize();
        mHeaderSizes[LayerT::LAYER_TRANSPORT] = transportHeader->headerSize();
        index += transportHeader->headerSize();
    }

    uint32_t PacketAnalyser::sizeToLayer(LayerT topmostLayer) const
    {
        assert(topmostLayer < LayerT::NUM_LAYERS);

        uint32_t acc{0};
        for (uint64_t iii = 0; iii <= topmostLayer; ++iii)
        {
            acc += mHeaderSizes[iii];
        }

        return acc;
    }
}
