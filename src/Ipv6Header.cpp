/**
 * @file Ipv6Header.cpp
 * @author Tomas Polasek
 * @brief Class for containing information about IPv6 header.
 */

#include "Ipv6Header.h"

namespace pa
{
    Ipv6Header::Ipv6Header(const uint8_t *headerStart, uint64_t remainingSize) :
        BaseNetworkHeader(sizeof(Ipv6HeaderForm), FilterT::IPV6)
    {
        if (remainingSize < mSize)
        {
            throw std::runtime_error("Unable to parse IPv6 header, not enough data!");
        }

        mIpv6Header = reinterpret_cast<const Ipv6HeaderForm*>(headerStart);

        switch(mIpv6Header->nextHeader)
        {
            case static_cast<uint8_t>(NetNextHeaderType::TCP):
            {
                mNextLayerType = FilterT::TCP;
                break;
            }
            case static_cast<uint8_t>(NetNextHeaderType::UDP):
            {
                mNextLayerType = FilterT::UDP;
                break;
            }
            case static_cast<uint8_t>(NetNextHeaderType::DEST):
            {
                break;
            }
            case static_cast<uint8_t>(NetNextHeaderType::HOP):
            {
                break;
            }
            case static_cast<uint8_t>(NetNextHeaderType::ROUTING):
            {
                break;
            }
            default:
            {
                //throw std::runtime_error("Unknown next header in IPv6 header parsing.");
            }
        }

        mSrcAddress.setAddr(mIpv6Header->srcIp, FilterT::IPV6);
        mDestAddress.setAddr(mIpv6Header->destIp, FilterT::IPV6);
    }

    void Ipv6Header::printContent() const
    {
        std::cout << "Content of the IPv6 header : \n" <<
                     "\tpayloadLength \t: " << ntohs(mIpv6Header->payloadLength) << "\n" <<
                     "\ttotalLength \t: " << getTotalSize() << "\n" <<
                     "\tnextHeader \t: " << static_cast<uint16_t>(mIpv6Header->nextHeader);
        switch (mNextLayerType)
        {
            case FilterT::TCP:
            {
                std::cout << " (TCP)\n";
                break;
            }
            case FilterT::UDP:
            {
                std::cout << " (UDP)\n";
                break;
            }
            default:
            {
                std::cout << " (UNK)\n";
                break;
            }
        }
        std::cout << "\thopLimit \t: " << static_cast<uint16_t>(mIpv6Header->hopLimit) << "\n" <<
                     "\tsrcIp \t\t: ";

        std::cout << std::hex;
        for (uint8_t iii = 0; iii < 16; ++iii)
        {
            printf("%02X", static_cast<uint16_t>(mIpv6Header->srcIp[iii]));
            if ((iii+1) % 2 == 0 && iii && iii != 15)
            {
                std::cout << ":";
            }
        }
        std::cout << std::dec;

        std::cout << "\n\tdestIp \t\t: ";

        std::cout << std::hex;
        for (uint8_t iii = 0; iii < 16; ++iii)
        {
            printf("%02X", static_cast<uint16_t>(mIpv6Header->destIp[iii]));
            if ((iii + 1) % 2 == 0 && iii && iii != 15)
            {
                std::cout << ":";
            }
        }
        std::cout << std::dec;

        std::cout << std::endl;
    }

    uint64_t Ipv6Header::getTotalSize() const
    {
        return ntohs(mIpv6Header->payloadLength) + sizeof(Ipv6HeaderForm);
    }
}
