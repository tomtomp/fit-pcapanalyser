/**
 * @file ArpHeader.cpp
 * @author Tomas Polasek
 * @brief Class for containing information about ARP header.
 */

#include "ArpHeader.h"

namespace pa
{
    ArpHeader::ArpHeader(const uint8_t *headerStart, uint64_t remainingSize) :
        BaseNetworkHeader(sizeof(ArpHeaderForm), FilterT::ARP)
    {
        if (remainingSize < mSize)
        {
            throw std::runtime_error("Unable to parse ARP header, not enough data!");
        }

        mArpHeader = reinterpret_cast<const ArpHeaderForm*>(headerStart);

        // Calculate the correct size.
        mSize = sizeof(ArpHeaderForm) + 2 * mArpHeader->hwAddressLength + 2 * mArpHeader->protAddressLength;

        if (remainingSize < mSize)
        {
            throw std::runtime_error("Unable to parse ARP header, not enough data!");
        }

        mNextLayerType = FilterT::UNK;
    }

    void ArpHeader::printContent() const
    {
        std::cout << "Content of the ARP header : \n" <<
                  "\thwAddressLength: " << mArpHeader->hwAddressLength << "\n" <<
                  "\tprotAddressLength: " << mArpHeader->protAddressLength << std::endl;
    }

    uint64_t ArpHeader::getTotalSize() const
    {
        return mSize;
    }
}
