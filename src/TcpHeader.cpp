/**
 * @file TcpHeader.cpp
 * @author Tomas Polasek
 * @brief Class for containing information about TCP header.
 */

#include "TcpHeader.h"

namespace pa
{

    TcpHeader::TcpHeader(const uint8_t *headerStart, uint64_t remainingSize) :
        BaseNetworkHeader(sizeof(TcpHeaderForm), FilterT::TCP)
    {
        if (remainingSize < mSize)
        {
            throw std::runtime_error("Unable to parse TCP header, not enough data!");
        }

        mTcpHeader = reinterpret_cast<const TcpHeaderForm*>(headerStart);

        uint64_t dataOffset{static_cast<uint64_t>(ntohs(mTcpHeader->dataOffsetFlags)) >> DATA_OFFSET_OFFSET};

        // Calculate the correct size.
        mSize = dataOffset * DATA_OFFSET_MULTIPLIER;

        if (mSize < DATA_OFFSET_MIN * DATA_OFFSET_MULTIPLIER)
        {
            throw std::runtime_error("Unable to parse TCP header, data offset < 5!");
        }

        if (remainingSize < mSize)
        {
            throw std::runtime_error("Unable to parse TCP header, not enough data!");
        }

        mSrcAddress.setAddr(mTcpHeader->srcPort, FilterT::TCP);
        mDestAddress.setAddr(mTcpHeader->destPort, FilterT::TCP);
    }

    void TcpHeader::printContent() const
    {
        uint64_t dataOffset{static_cast<uint64_t>(ntohs(mTcpHeader->dataOffsetFlags)) >> DATA_OFFSET_OFFSET};
        std::cout << "Content of the TCP header : \n" <<
                  "\tsrcPort \t: " << ntohs(*reinterpret_cast<const uint16_t*>(mTcpHeader->srcPort)) << "\n" <<
                  "\tdestPort \t: " << ntohs(*reinterpret_cast<const uint16_t*>(mTcpHeader->destPort)) << "\n" <<
                  "\tseqNum \t\t: " << ntohl(mTcpHeader->seqNum) << "\n" <<
                  "\tackNum \t\t: " << ntohl(mTcpHeader->ackNum) << "\n" <<
                  "\tdataOffset \t: " << dataOffset <<
                  " (" << mSize << " B)" << std::endl;
    }
}
