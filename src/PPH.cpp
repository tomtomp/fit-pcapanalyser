/**
 * @file PPH.cpp
 * @author Tomas Polasek
 * @brief Pcap packet header.
 */

#include "PPH.h"

namespace pa
{
    PPH::PPH(std::istream *inputStream, const PGH &globalHeader)
    {
        char *readPtr{reinterpret_cast<char*>(&mHeader)};
        inputStream->read(readPtr, sizeof(PcapPacketHeaderForm));
        if (!(*inputStream))
        {
            throw std::runtime_error("Unable to read whole pcap packet header!");
        }

        mNanoS = globalHeader.nanoS();
        if (globalHeader.shouldSwap())
        {
            nibbleSwapper(mHeader.tsSec);
            nibbleSwapper(mHeader.tsUsec);
            nibbleSwapper(mHeader.inclLen);
            nibbleSwapper(mHeader.origLen);
        }

        mData.resize(mHeader.inclLen);

        char *writePtr{reinterpret_cast<char*>(&(mData[0]))};
        inputStream->read(writePtr, mHeader.inclLen);

        if (!(*inputStream))
        {
            throw std::runtime_error("Unable to read whole packet data!");
        }

        mPacketAnalyser.analyse(mData);
    }

    void PPH::printContent() const
    {
        double usec{mNanoS ? mHeader.tsUsec / 1000.0 : mHeader.tsUsec};

        std::cout << "Content of pcap packet header : \n" <<
                     "\ttimestamp \t: " << mHeader.tsSec << ":" << usec << "\n" <<
                     "\tinclLen \t: " << mHeader.inclLen << "\n" <<
                     "\torigLen \t: " << mHeader.origLen << std::endl;

        mPacketAnalyser.printContent();
    }

    uint32_t PPH::sizeFromLayer(LayerT topmostLayer) const
    {
        return origSize() - mPacketAnalyser.sizeToLayer(topmostLayer);
    }
}
