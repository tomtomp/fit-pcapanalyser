/**
 * @file NetAddress.cpp
 * @author Tomas Polasek
 * @brief Class used for holding addresses for any network layer.
 */

#include "NetAddress.h"

namespace pa
{
    std::ostream &operator<<(std::ostream &out, const NetAddress &rhs)
    {
        const std::vector<uint8_t> &address{rhs.mAddrBuffer};
        FilterT type{rhs.mAddrType};

        assert(address.size() == sizeFromFilter(type));

        switch (type)
        {
            case FilterT::ETH:
            {
                out << std::hex;

                for (uint64_t iii = 0; iii < ETH_ADDRESS_SIZE; ++iii)
                {
                    out << std::setfill('0') << std::setw(2) << static_cast<uint16_t>(address[iii]) << (iii == ETH_ADDRESS_SIZE - 1 ? "\0" : ":");
                }

                out << std::setfill('\0') << std::setw(0) << std::dec;

                break;
            }
            case FilterT::IPV4:
            {
                for (uint64_t iii = 0; iii < IPV4_ADDRESS_SIZE; ++iii)
                {
                    out << static_cast<uint16_t>(address[iii]) << (iii == IPV4_ADDRESS_SIZE - 1 ? "\0" : ".");
                }

                break;
            }
            case FilterT::IPV6:
            {
                out << std::hex;

                for (uint64_t iii = 0; iii < IPV6_ADDRESS_SIZE; ++iii)
                {
                    //printf("%02X", static_cast<uint16_t>(address[iii]));
                    out << std::setfill('0') << std::setw(2) << static_cast<uint16_t>(address[iii]);
                    if ((iii + 1) % 2 == 0 && iii && iii != IPV6_ADDRESS_SIZE - 1)
                    {
                        out << ":";
                    }
                }

                out << std::setfill('\0') << std::setw(0) << std::dec;

                break;
            }
            case FilterT::TCP:
            {
                out << ntohs(*reinterpret_cast<const uint16_t*>(&address[0]));

                break;
            }
            case FilterT::UDP:
            {
                out << ntohs(*reinterpret_cast<const uint16_t*>(&address[0]));

                break;
            }
            default:
            {
                out << "UNK";

                break;
            }
        }
        return out;
    }
}
