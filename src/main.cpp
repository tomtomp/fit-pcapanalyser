/**
 * @file main.cpp
 * @author Tomas Polasek
 * @brief Main file for thsi project.
 */

#include "main.h"

int main(int argc, char *argv[])
{
    try
    {
        pa::AnalyserApp app(argc, argv);

        app.run();
    } catch (std::runtime_error &err)
    {
        std::cerr << "Error occurred : \n" << err.what() << std::endl;
    }
}
