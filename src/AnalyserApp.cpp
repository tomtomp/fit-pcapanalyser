/**
 * @file AnalyserApp.cpp
 * @author Tomas Polasek
 * @brief Main analyser application class.
 */

#include "AnalyserApp.h"

namespace pa
{
    AnalyserApp::AnalyserApp(int argc, char *argv[]) :
        mSettings(argc, argv),
        mInput(&std::cin)
    {
        if (!mSettings.inputFile().empty())
        {
            mInputFile.open(mSettings.inputFile(), std::ios::binary);
            if (!mInputFile)
            {
                throw std::runtime_error("Error, unable to open input file!");
            }
            mInput = &mInputFile;
        }

        if (!mInput)
        {
            throw std::runtime_error("Error, unable to open the input stream!");
        }
    }

    void AnalyserApp::run()
    {
        PGH globalPcapHeader(mInput);
        if (mSettings.verbose())
        {
            globalPcapHeader.printContent();
        }

        std::vector<PPH> packets;

        const Filter &filter{mSettings.filter()};

        while (mInput->peek() != EOF)
        { // Read in the packets.
            PPH packet{mInput, globalPcapHeader};
            if (filter.match(packet.packetAnalyser().headers()))
            {
                packets.emplace_back(std::move(packet));
            }
        }
        if (mSettings.verbose())
        {
            std::cout << packets.size() << " packets matched the filter!" << std::endl;
        }

        /*
        for (uint64_t iii = 0; iii < packets.size(); ++iii)
        {
            const PPH &packetHeader(packets[iii]);

            if (mSettings.verbose() && filter.match(packetHeader.packetAnalyser().headers()))
            {
                std::cout << "No. " << iii << std::endl;
                packetHeader.printContent();
            }
        }
         */

        if (mSettings.filter().top10())
        {
            runTop10(packets);
        }
        else
        {
            runNormal(packets);
        }
    }

    void AnalyserApp::runTop10(const std::vector<PPH> &packets)
    {
        const Filter &filter{mSettings.filter()};
        FilterT type{filter.top10Type()};
        LayerT layer{layerFromFilter(type)};

        if (layer >= LayerT::NUM_LAYERS)
        {
            throw std::runtime_error("Unknown network layer!");
        }

        struct ValueAccumulator
        {
            uint64_t accumulator1{0};
            uint64_t accumulator2{0};
        };

        std::map<NetAddress, ValueAccumulator> srcMap;
        std::map<NetAddress, ValueAccumulator> destMap;

        uint64_t counter{0};

        for (const auto &h : packets)
        {
            const auto &headers(h.packetAnalyser().headers());

            assert(filter.match(headers));
            assert(layer < headers.size() && headers[layer]->layerType() == type);

            if (mSettings.verbose())
            {
                std::cout << "Packet No. " << counter << std::endl;
                h.printContent();
            }

            if (filter.sourceFilter())
            {
                ValueAccumulator &acc(srcMap[headers[layer]->src()]);
                acc.accumulator1 += h.origSize();
                acc.accumulator2 += h.sizeFromLayer(filter.topmostLayer());
            }

            if (filter.destinationFilter())
            {
                ValueAccumulator &acc(destMap[headers[layer]->dest()]);
                acc.accumulator1 += h.origSize();
                acc.accumulator2 += h.sizeFromLayer(filter.topmostLayer());
            }
            /*
            if (filter.match(headers))
            { // The packets have to match the filter.
                if (layer < headers.size() && headers[layer]->layerType() == type)
                { // And correct protocol on top10 layer has to be used.
                }
            }
             */

            counter++;
        }

        // Vectors used for sorting the values.
        std::vector<std::pair<NetAddress, ValueAccumulator>> srcVec;
        std::vector<std::pair<NetAddress, ValueAccumulator>> destVec;

        //auto fun{
        std::function<void (const decltype(srcMap)&, decltype(srcVec)&)> fun(
            [](const decltype(srcMap) &map,
               decltype(srcVec) &vec)
            {
                for (const auto &p : map)
                {
                    decltype(vec.begin()) loc{std::lower_bound(vec.begin(), vec.end(), p,
                                              [&](const std::pair<NetAddress, ValueAccumulator> &p1,
                                                  const std::pair<NetAddress, ValueAccumulator> &p2)
                    {
                        return p1.second.accumulator1 > p2.second.accumulator1;
                    })};

                    if (vec.size() < 10)
                    {
                        vec.insert(loc, p);
                    }
                    else if (loc != vec.end())
                    {
                        vec.insert(loc, p);
                        vec.pop_back();
                    }
                }
            }
        );

        // Sorted insert into the vectors.
        if (filter.sourceFilter())
        {
            fun(srcMap, srcVec);
        }
        if (filter.destinationFilter())
        {
            fun(destMap, destVec);
        }

        /*
        std::sort(srcVec.begin(), srcVec.end(), [&](std::pair<NetAddress, ValueAccumulator> &p1,
                                                    std::pair<NetAddress, ValueAccumulator> &p2)
        {
            return p1.second.accumulator1 > p2.second.accumulator1;
        });

        std::sort(destVec.begin(), destVec.end(), [&](std::pair<NetAddress, ValueAccumulator> &p1,
                                                      std::pair<NetAddress, ValueAccumulator> &p2)
        {
            return p1.second.accumulator1 > p2.second.accumulator1;
        });
         */

        if (filter.sourceFilter())
        { // Print first 10 records from the source vector.
            assert(srcVec.size() <= 10);
            for (const auto &r : srcVec)
            {
                std::cout << r.first << " " <<
                             r.second.accumulator1 << " " <<
                             r.second.accumulator2 << std::endl;
            }
        }

        if (filter.destinationFilter())
        { // Print first 10 records from the destination vector.
            assert(destVec.size() <= 10);
            for (const auto &r : destVec)
            {
                std::cout << r.first << " " <<
                          r.second.accumulator1 << " " <<
                          r.second.accumulator2 << std::endl;
            }
        }
    }

    void AnalyserApp::runNormal(const std::vector<PPH> &packets)
    {
        const Filter &filter{mSettings.filter()};

        uint32_t accumulator1{0};
        uint32_t accumulator2{0};

        uint64_t counter{0};

        for (const auto &h : packets)
        {
            assert(filter.match(h.packetAnalyser().headers()));

            if (mSettings.verbose())
            {
                std::cout << "Packet No. " << counter << std::endl;
                h.printContent();
            }
            accumulator1 += h.origSize();
            accumulator2 += h.sizeFromLayer(filter.topmostLayer());

            /*
            if (filter.match(h.packetAnalyser().headers()))
            {
            }
             */

            counter++;
        }

        std::cout << accumulator1 << " " << accumulator2 << std::endl;
    }
}
