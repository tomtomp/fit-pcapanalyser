/**
 * @file Filter.h
 * @author Tomas Polasek
 * @brief Filter class possibly containing multiple sub-filters.
 */

#ifndef PCAPANALYSER_FILTER_H
#define PCAPANALYSER_FILTER_H

#include <vector>
#include <cstring>
#include <memory>

#include "Types.h"
#include "BaseNetworkHeader.h"

namespace pa
{
    class Filter
    {
    public:

        /**
         * Create default empty filter, everything passes through it.
         */
        Filter();

        /**
         * Add a filter with given type and address value.
         * @param type Type of the filter.
         * @param address The filter address.
         */
        void addFilter(FilterT type, const NetAddress &address);

        /**
         * Check the vector of headers in a packet against the rules
         * contained in this filter.
         * @param headers Vector of headers.
         */
        bool match(const std::vector<std::unique_ptr<BaseNetworkHeader>> &headers) const;

        /**
         * Set this filter to top10 mode.
         * @param type Type of the filter.
         * @return Returns true, if it was possible to set top10 filter.
         */
        bool setTop10Filter(FilterT type);

        void setSourceFilter()
        { mSourceFilter = true; }
        void setDestinationFilter()
        { mDestinationFilter = true; }

        bool sourceFilter() const
        { return mSourceFilter; }
        bool destinationFilter() const
        { return mDestinationFilter; }
        bool top10() const
        { return mTop10Mode; }
        FilterT top10Type() const
        { return mTop10Type; }

        LayerT topmostLayer() const
        { return mTopmostLayer; }
    private:
        /// Is this filter in top10 mode?
        bool mTop10Mode{false};

        /**
         * If the filter is in top10 mode, this variable contains
         * the type.
         */
        FilterT mTop10Type{FilterT::UNK};

        /// Filter using source address?
        bool mSourceFilter{false};

        /// Filter using destination address?
        bool mDestinationFilter{false};

        class FilterElement;

        /**
         * Array of vectors for the filtering rules.
         */
        std::vector<FilterElement> mRules[LayerT::NUM_LAYERS];

        /// What is the topmost layer, this filter has rules for?
        LayerT mTopmostLayer{LayerT::LAYER_LINK};

        /**
         * Contains a single filtering rule.
         */
        class FilterElement
        {
        public:
            /**
             * Create the filter.
             * @param filterType Type of the filter.
             * @param address The filter address.
             */
            FilterElement(FilterT filterType, const NetAddress &address) :
                mType{filterType},
                mAddress{address}
            { }

            /**
             * Compare the contained value with the given value.
             * @param rhs What to compare the value with.
             * @return Returns true, if the values are the same.
             */
            bool compare(const NetAddress &rhs) const
            {
                return mAddress == rhs;
            }

            FilterT type() const
            { return mType; }
        private:
            /// On which layer/protocol does this filter work on?
            FilterT mType;
            /// Address container.
            NetAddress mAddress;
        protected:
        };
    protected:
    };
}

#endif //PCAPANALYSER_FILTER_H
