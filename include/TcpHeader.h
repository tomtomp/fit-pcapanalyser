/**
 * @file TcpHeader.h
 * @author Tomas Polasek
 * @brief Class for containing information about TCP header.
 */

#ifndef PCAPANALYSER_TCPHEADER_H
#define PCAPANALYSER_TCPHEADER_H

#include "BaseNetworkHeader.h"

namespace pa
{
    class TcpHeader : public BaseNetworkHeader
    {
    public:
        /// Offset of the data offset in the header.
        static constexpr uint64_t DATA_OFFSET_OFFSET{12};
        /// Multiplier of the data offset.
        static constexpr uint64_t DATA_OFFSET_MULTIPLIER{sizeof(uint32_t)};
        /// Minimal value of the data offset.
        static constexpr uint64_t DATA_OFFSET_MIN{5};

        /**
         * Analyse given TCP header.
         * @param headerStart Where does the header start at.
         * @param remainingSize Number of bytes in the header buffer.
         */
        TcpHeader(const uint8_t *headerStart, uint64_t remainingSize);

        /// Print content of this header.
        virtual void printContent() const override final;
    private:
        /// Container for the TCP header.
        const TcpHeaderForm *mTcpHeader{nullptr};
    protected:
    };
}

#endif //PCAPANALYSER_TCPHEADER_H
