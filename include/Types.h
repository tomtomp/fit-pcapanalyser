/**
 * @file types.h
 * @author Tomas Polasek
 * @brief Types for the pcap analyser.
 */

#ifndef PCAPANALYSER_TYPES_H
#define PCAPANALYSER_TYPES_H

#include <cstdint>
#include <cassert>
#include <stdexcept>

extern "C"
{
#   include <arpa/inet.h>
#   include <net/ethernet.h>
}

namespace pa
{
    /// Maximal length of the filter value in Bytes.
    static constexpr uint64_t MAX_FILTER_BYTES{16};

    /// Available types of the filter.
    enum class FilterT
    {
        UNK = 0,
        ETH,
        IPV4,
        IPV6,
        TCP,
        UDP,
        ARP,
        SIZE
    };

    /// Size of IPv4 address.
    static constexpr uint64_t ETH_ADDRESS_SIZE{6};
    /// Size of IPv6 address.
    static constexpr uint64_t IPV6_ADDRESS_SIZE{16};
    /// Size of IPv4 address.
    static constexpr uint64_t IPV4_ADDRESS_SIZE{4};
    /// Size of TCP address.
    static constexpr uint64_t TCP_ADDRESS_SIZE{2};
    /// Size of UDP address.
    static constexpr uint64_t UDP_ADDRESS_SIZE{2};

    /**
     * Structure of the global pcap header.
     * Found on : https://wiki.wireshark.org/Development/LibpcapFileFormat
     */
    struct PcapGlobalHeaderForm
    {
        uint32_t magicNumber;   /* magic number */
        uint16_t versionMajor;  /* major version number */
        uint16_t versionMinor;  /* minor version number */
        int32_t  thiszone;       /* GMT to local correction */
        uint32_t sigfigs;        /* accuracy of timestamps */
        uint32_t snaplen;        /* max length of captured packets, in octets */
        uint32_t network;        /* data link type */
    };

    /**
     * Structure of the packet header.
     * Found on : https://wiki.wireshark.org/Development/LibpcapFileFormat
     */
    struct PcapPacketHeaderForm
    {
        uint32_t tsSec;         /* timestamp seconds */
        uint32_t tsUsec;        /* timestamp microseconds */
        uint32_t inclLen;       /* number of octets of packet saved in file */
        uint32_t origLen;       /* actual length of packet */
    };

    /**
     * Structure of the Ethernet header.
     */
    struct EthernetHeaderForm
    {
        uint8_t destMac[6];        /* Destination MAC address */
        uint8_t srcMac[6];         /* Source MAC address */
        uint16_t type;             /* Type of contained protocol */
    };

    /// Values for the next header in IPv4 and IPv6.
    enum class NetNextHeaderType
    {
        HOP = 0,
        TCP = 6,
        UDP = 17,
        ROUTING = 43,
        DEST = 60,
    };

    /**
     * Structure of IPv6 header.
     * Found on : https://en.wikipedia.org/wiki/IPv6_packet
     */
    struct Ipv6HeaderForm
    {
        uint32_t verTrafficFlow;            /* Version (4b), Traffic (8b) and Flow (20b) information */
        uint16_t payloadLength;             /* Size of the payload in octets */
        uint8_t nextHeader;                 /* Type of the next header */
        uint8_t hopLimit;                   /* Hop limit */
        uint8_t srcIp[IPV6_ADDRESS_SIZE];   /* Source IPv6 address */
        uint8_t destIp[IPV6_ADDRESS_SIZE];  /* Destination IPv6 address */
    };

    /**
     * Structure of IPv4 header.
     * Found on : https://en.wikipedia.org/wiki/IPv4
     */
    struct Ipv4HeaderForm
    {
        uint8_t versionIhl;                 /* Version and header length information */
        uint8_t dspcEcn;                    /* QOS things */
        uint16_t totalLength;               /* Total length of the packet from network layer up */
        uint16_t identification;            /* Fragment identification */
        uint16_t flagsFragmentOffset;       /* Fragment flags and offset */
        uint8_t ttl;                        /* Time to live */
        uint8_t protocol;                   /* Protocol used on the layer above */
        uint16_t headerChecksum;            /* Checksum for the IPv4 header */
        uint8_t srcIp[IPV4_ADDRESS_SIZE];   /* Source IPv4 address */
        uint8_t destIp[IPV4_ADDRESS_SIZE];  /* Destination IPv4 address */
    };

    /**
     * Structure of TCP header.
     * Found on : https://en.wikipedia.org/wiki/Transmission_Control_Protocol
     */
    struct TcpHeaderForm
    {
        uint8_t srcPort[TCP_ADDRESS_SIZE];  /* Source port */
        uint8_t destPort[TCP_ADDRESS_SIZE]; /* Destination port */
        uint32_t seqNum;                    /* Sequence number */
        uint32_t ackNum;                    /* Acknowledgment number */
        uint16_t dataOffsetFlags;           /* Top 4 bits define length of this header */
        uint16_t windowSize;                /* Size of the receive window */
        uint16_t checksum;                  /* Header checksum */
        uint16_t urgentPtr;                 /* Ptr for urgent data */
    };

    /**
     * Structure of UDP header.
     * Found on : https://en.wikipedia.org/wiki/User_Datagram_Protocol
     */
    struct UdpHeaderForm
    {
        uint8_t srcPort[UDP_ADDRESS_SIZE];  /* Source port */
        uint8_t destPort[UDP_ADDRESS_SIZE]; /* Destination port */
        uint16_t length;                    /* Length of the header + data */
        uint16_t checksum;                  /* Header checksum */
    };

    /**
     * Structure of ARP header.
     * Found on : https://en.wikipedia.org/wiki/Address_Resolution_Protocol#Packet_structure
     */
    struct ArpHeaderForm
    {
        uint16_t hwType;            /* Hardware type, 1 for ETH */
        uint16_t protType;          /* Protocol type, 0x0800 for IPv4 */
        uint8_t hwAddressLength;    /* Length of the hardware address in bytes */
        uint8_t protAddressLength;  /* Length of the protocol address in bytes */
        uint16_t operation;         /* Operation code */
        /* Sender Hardware address */
        /* Sender Protocol address */
        /* Target Hardware address */
        /* Target Protocol address */
    };

    /// Layer identification.
    enum LayerT
    {
        LAYER_LINK = 0,
        LAYER_NETWORK,
        LAYER_TRANSPORT,
        NUM_LAYERS
    };
}

#endif //PCAPANALYSER_TYPES_H
