/**
 * @file Ipv6Header.h
 * @author Tomas Polasek
 * @brief Class for containing information about IPv6 header.
 */

#ifndef PCAPANALYSER_IPV6HEADER_H
#define PCAPANALYSER_IPV6HEADER_H

#include "BaseNetworkHeader.h"

namespace pa
{
    class Ipv6Header : public BaseNetworkHeader
    {
    public:

        /**
         * Analyse the given IPv6 header.
         * @param headerStart Pointer to the beginning of the header.
         * @param remainingSize Number of bytes in the header buffer.
         */
        Ipv6Header(const uint8_t *headerStart, uint64_t remainingSize);

        /// Get the total size of this packet.
        uint64_t getTotalSize() const;

        /// Print content of this header.
        virtual void printContent() const override final;
    private:
        /// Container for the Ipv6 header.
        const Ipv6HeaderForm *mIpv6Header{nullptr};
    protected:
    };
}

#endif //PCAPANALYSER_IPV6HEADER_H
