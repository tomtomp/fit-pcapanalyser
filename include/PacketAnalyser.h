/**
 * @file PacketAnalyser.h
 * @author Tomas Polasek
 * @brief Used for analysing data contained within a packet.
 */

#ifndef PCAPANALYSER_PACKETANALYSER_H
#define PCAPANALYSER_PACKETANALYSER_H

#include <vector>
#include <memory>

#include "Types.h"
#include "EthernetHeader.h"
#include "Ipv6Header.h"
#include "Ipv4Header.h"
#include "TcpHeader.h"
#include "UdpHeader.h"
#include "ArpHeader.h"

namespace pa
{
    class PacketAnalyser
    {
    public:
        /// Minimal frame size in bytes.
        static constexpr uint64_t MIN_FRAME_SIZE{60};

        /**
         * Prepare the packet analyser object.
         */
        PacketAnalyser();

        /**
         * Analyse packet information in given packet header/data.
         * @param data Vector of binary data, containing the Ethernet
         *   header from the index 0.
         */
        void analyse(const std::vector<uint8_t> &data);

        /// Print content of the packet in human readable form.
        void printContent() const;

        const std::vector<std::unique_ptr<BaseNetworkHeader>> &headers() const
        { return mHeaders; }

        /**
         * Get the size of all headers upto and included
         * to the topmostLayer.
         * @param topmostLayer Identifier of the layer.
         */
        uint32_t sizeToLayer(LayerT topmostLayer) const;
    private:
        /// Container for the inner headers.
        std::vector<std::unique_ptr<BaseNetworkHeader>> mHeaders;

        /// Sizes of headers in bytes.
        uint64_t mHeaderSizes[LayerT::NUM_LAYERS];
    protected:
    };
}

#endif //PCAPANALYSER_PACKETANALYSER_H
