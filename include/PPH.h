/**
 * @file PPH.h
 * @author Tomas Polasek
 * @brief Pcap packet header.
 */

#ifndef PCAPANALYSER_PPH_H
#define PCAPANALYSER_PPH_H

#include <memory>
#include <vector>
#include <iterator>

#include "PGH.h"
#include "PacketAnalyser.h"

namespace pa
{
    class PPH
    {
    public:
        /**
         * Read the data from given input stream and interpret it.
         * @param inputStream Input stream containing the pcap file.
         * @param globalHeader The global header of this file.
         */
        PPH(std::istream *inputStream, const PGH &globalHeader);

        const std::vector<uint8_t> &data() const
        { return mData; }
        uint32_t includedSize() const
        { return mHeader.inclLen; }
        uint32_t origSize() const
        { return mHeader.origLen; }
        const PacketAnalyser &packetAnalyser() const
        { return mPacketAnalyser; }

        /**
         * Get the size of this packet from given layer up.
         * Topmost layer is excluded.
         * @param topmostLayer Identifier of the layer.
         */
        uint32_t sizeFromLayer(LayerT topmostLayer) const;

        /// Print out the header data.
        void printContent() const;
    private:
        /// Container for the binary data.
        PcapPacketHeaderForm mHeader;

        /// Container for the packet binary data.
        std::vector<uint8_t> mData;

        /// Are the times given in nano seconds?
        bool mNanoS{false};

        /// Analyser used for analysing the packet content.
        PacketAnalyser mPacketAnalyser;
    protected:
    };
}

#endif //PCAPANALYSER_PPH_H
