/**
 * @file ArpHeader.h
 * @author Tomas Polasek
 * @brief Class for containing information about ARP header.
 */

#ifndef PCAPANALYSER_ARPHEADER_H
#define PCAPANALYSER_ARPHEADER_H

#include "BaseNetworkHeader.h"

namespace pa
{
    class ArpHeader : public BaseNetworkHeader
    {
    public:
        /**
         * Analyse the given ARP header.
         * @param headerStart Pointer to the beginning of the header.
         * @param remainingSize Number of bytes in the header buffer.
         */
        ArpHeader(const uint8_t *headerStart, uint64_t remainingSize);

        /// Get the total size of this packet.
        uint64_t getTotalSize() const;

        /// Print content of this header.
        virtual void printContent() const override final;
    private:
        /// Container for the ARP header.
        const ArpHeaderForm *mArpHeader{nullptr};
    protected:
    };
}

#endif //PCAPANALYSER_ARPHEADER_H
