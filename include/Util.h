/**
 * @file Util.h
 * @author Tomas Polasek
 * @brief Utility functions.
 */

#ifndef PCAPANALYSER_UTIL_H
#define PCAPANALYSER_UTIL_H

#include "Types.h"

namespace pa
{
    /**
     * Swap given variable to the other endian.
     * Variable has to have even number of bytes.
     * @param val Value to convert.
     */
    template <typename T, uint64_t SIZE = sizeof(T)>
    static inline void nibbleSwapper(T &val)
    {
        for (uint64_t iii = 0; iii < SIZE / 2; ++iii)
        {
            std::swap((reinterpret_cast<uint8_t*>(&val))[iii],
                      (reinterpret_cast<uint8_t*>(&val))[SIZE - iii - 1]);
        }
    }

    /**
     * Swap given variable to the other endian.
     * Variable has to have even number of bytes.
     * @param val Value to convert.
     */
    template <typename T, uint64_t SIZE = sizeof(T)>
    static inline void nibbleSwapper(T *val)
    {
        for (uint64_t iii = 0; iii < SIZE / 2; ++iii)
        {
            std::swap((reinterpret_cast<uint8_t*>(val))[iii],
                      (reinterpret_cast<uint8_t*>(val))[SIZE - iii - 1]);
        }
    }

    /**
     * Convert filter type to the layer type.
     * @param filter The filter type.
     * @return Filter type converted to layer type.
     */
    static inline LayerT layerFromFilter(FilterT filter)
    {
        switch(filter)
        {
            case FilterT::ETH:
            {
                return LayerT::LAYER_LINK;
            }
            case FilterT::IPV4:
            {
                return LayerT::LAYER_NETWORK;
            }
            case FilterT::IPV6:
            {
                return LayerT::LAYER_NETWORK;
            }
            case FilterT::TCP:
            {
                return LayerT::LAYER_TRANSPORT;
            }
            case FilterT::UDP:
            {
                return LayerT::LAYER_TRANSPORT;
            }
            default:
            {
                return LayerT::LAYER_LINK;
            }
        }
    }

    /**
     * Convert filter type to corresponding address size in bytes.
     * @param filter Filter type.
     * @return Size of the address in bytes.
     */
    static inline uint64_t sizeFromFilter(FilterT filter)
    {
        switch(filter)
        {
            case FilterT::ETH:
            {
                return ETH_ADDRESS_SIZE;
            }
            case FilterT::IPV4:
            {
                return IPV4_ADDRESS_SIZE;
            }
            case FilterT::IPV6:
            {
                return IPV6_ADDRESS_SIZE;
            }
            case FilterT::TCP:
            {
                return TCP_ADDRESS_SIZE;
            }
            case FilterT::UDP:
            {
                return UDP_ADDRESS_SIZE;
            }
            default:
            {
                return 0;
            }
        }
    }
}

#endif //PCAPANALYSER_UTIL_H
