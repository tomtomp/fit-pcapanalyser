/**
 * @file Address.h
 * @author Tomas Polasek
 * @brief Class used for holding addresses for any network layer.
 */

#ifndef PCAPANALYSER_NETADDRESS_H
#define PCAPANALYSER_NETADDRESS_H

#include <vector>
#include <cstring>
#include <iostream>
#include <iomanip>

#include "Types.h"
#include "Util.h"

namespace pa
{
    class NetAddress
    {
    public:
        /**
         * Create an empty network address container.
         */
        NetAddress()
        { }

        /**
         * Copy the addres from given buffer.
         * @param addressBuffer Buffer containing the address, has
         *   to be at least addressSize long.
         * @param addressType Type of the address.
         */
        NetAddress(const uint8_t *addressBuffer, FilterT addressType)
        {
            setAddr(addressBuffer, addressType);
        }

        /**
         * Copy the address from given buffer.
         * @param addressBuffer Buffer containing the address, has
         *   to be at least addressSize long.
         * @param addressType Type of the address.
         */
        void setAddr(const uint8_t *addressBuffer, FilterT addressType)
        {
            mAddrType = addressType;
            mAddrBuffer.assign(addressBuffer, addressBuffer + sizeFromFilter(addressType));
        }

        friend std::ostream &operator<<(std::ostream &out, const NetAddress &rhs);

        bool operator<(const NetAddress &rhs) const
        {
            /*
            if (mAddrBuffer.size() != rhs.mAddrBuffer.size())
            {
                throw std::runtime_error("Cannot compare 2 addresses with different lengths!");
            }
             */

            if (mAddrType != rhs.mAddrType)
            {
                throw std::runtime_error("Cannot compare 2 addresses of different types!");
            }

            return mAddrBuffer < rhs.mAddrBuffer;
        }

        bool operator==(const NetAddress &rhs) const
        {
            if (mAddrType != rhs.mAddrType)
            {
                throw std::runtime_error("Cannot compare 2 addresses of different types!");
            }

            return mAddrBuffer == rhs.mAddrBuffer;
        }
    private:
        /// Buffer for the address.
        std::vector<uint8_t> mAddrBuffer;
        /// Type of the contained address.
        FilterT mAddrType{FilterT::UNK};
    protected:
    };
}

#endif //PCAPANALYSER_NETADDRESS_H
