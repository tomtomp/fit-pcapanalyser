/**
 * @file AnalyserApp.h
 * @author Tomas Polasek
 * @brief Main analyser application class.
 */

#ifndef PCAPANALYSER_ANALYSERAPP_H
#define PCAPANALYSER_ANALYSERAPP_H

#include <istream>
#include <fstream>
#include <map>
#include <algorithm>

#include "Settings.h"
#include "PGH.h"
#include "PPH.h"
#include "PacketAnalyser.h"

namespace pa
{
    class AnalyserApp
    {
    public:
        /**
         * Initialize the application.
         * @param argc Number of command line parameters.
         * @param argv Parameter vector.
         */
        AnalyserApp(int argc, char *argv[]);

        /**
         * Run the application.
         */
        void run();

        /**
         * Run top10 code.
         */
        void runTop10(const std::vector<PPH> &packets);

        /**
         * Run normal functions
         */
        void runNormal(const std::vector<PPH> &packets);
    private:
        /// Settings for this application.
        Settings mSettings;

        /// Input file.
        std::ifstream mInputFile;

        /// Input stream containing the pcap file.
        std::istream *mInput;
    protected:
    };
}

#endif //PCAPANALYSER_ANALYSERAPP_H
