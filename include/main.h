/**
 * @file main.h
 * @author Tomas Polasek
 * @brief Main file for this project.
 */

#ifndef PCAPANALYSER_MAIN_H
#define PCAPANALYSER_MAIN_H

#include <iostream>

#include "AnalyserApp.h"

#endif //PCAPANALYSER_MAIN_H
