/**
 * @file UdpHeader.h
 * @author Tomas Polasek
 * @brief Class for containing information about UDP header.
 */

#ifndef PCAPANALYSER_UDPHEADER_H
#define PCAPANALYSER_UDPHEADER_H

#include "BaseNetworkHeader.h"

namespace pa
{
    class UdpHeader : public BaseNetworkHeader
    {
    public:
        /**
         * Analyse given UDP header.
         * @param headerStart Where does the header start at.
         * @param remainingSize Number of bytes in the header buffer.
         */
        UdpHeader(const uint8_t *headerStart, uint64_t remainingSize);

        /// Print content of this header.
        virtual void printContent() const override final;
    private:
        /// Container for the TCP header.
        const UdpHeaderForm *mUdpHeader{nullptr};
    protected:
    };
}

#endif //PCAPANALYSER_UDPHEADER_H
