/**
 * @file Settings.h
 * @author Tomas Polasek
 * @brief Class used for parsing arguments passed by the user.
 */

#ifndef PCAPANALYSER_SETTINGS_H
#define PCAPANALYSER_SETTINGS_H

#include <iostream>
#include <exception>
#include <cstring>
#include <sstream>

#include "Types.h"
#include "Filter.h"

namespace pa
{
    class Settings
    {
    public:
        /// Minimal number of arguments
        static constexpr int64_t MIN_NUM_ARGS{6};

        /**
         * Parse the arguments.
         * @param argc Number of arguments.
         * @param argv Argument vector.
         */
        Settings(int argc, char *argv[]);

        const Filter &filter() const
        { return mFilter; }
        const std::string &inputFile() const
        { return mInputFile; }
        bool verbose() const
        { return mVerbose; }

        /// Translation record for filter type translation.
        struct TranslationRecord
        {
            TranslationRecord(const char* inStr, FilterT inVal) :
                str{inStr},
                val{inVal}
            { }
            const char* str{nullptr};
            FilterT val{FilterT::UNK};
        };
    private:
        /**
         * Parse command line arguments given by the user.
         * @param argc Number of arguments.
         * @param argv Argument vector.
         * @return Returns true, if the parsing ended successfuly, else
         *   returns false.
         */
        bool parseArguments(int argc, char *argv[]);

        /**
         * Print usage of this application.
         */
        void printUsage() const;

        /**
         * Translate filter type as string value to enum
         * @param str String representation of the filter type.
         * @return Filter type as enum.
         */
        FilterT translateFilterType(const char *str);

        /**
         * Parse the types and values passed by the user.
         * @param types String containing the types.
         * @param values String containing the values.
         * @return Returns true, if the parsing completed successfully.
         */
        bool parseFilterTypesValues(const char *types, const char *values);

        /**
         * Parse type/value pair and add corresponding filter rule.
         * @param type String representation of the filter type.
         * @param value String representation of the filter value.
         * @return Returns true, if the parsing was successful.
         */
        bool strParseFilterTypeValue(const std::string &type, const std::string &value);

        /// Translation of filter types string -> enum
        static const TranslationRecord FILTER_TYPE_TRANSLATE[];

        /// Filter rules.
        Filter mFilter;
        /// Input pcap file.
        std::string mInputFile;
        /// Is the application set to verbose mode?
        bool mVerbose{false};
    protected:
    };
}

#endif //PCAPANALYSER_SETTINGS_H
