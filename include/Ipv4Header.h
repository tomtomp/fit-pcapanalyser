/**
 * @file Ipv4Header.h
 * @author Tomas Polasek
 * @brief Class for containing information about IPv4 header.
 */

#ifndef PCAPANALYSER_IPV4HEADER_H
#define PCAPANALYSER_IPV4HEADER_H

#include "BaseNetworkHeader.h"

namespace pa
{
    class Ipv4Header : public BaseNetworkHeader
    {
    public:
        /// Multiplier for the length of the header.
        static constexpr uint64_t IHL_MULTIPLIER{sizeof(uint32_t)};
        /// Minimal value of the IHL.
        static constexpr uint64_t IHL_MIN{5};

        /**
         * Analyse the given IPv4 header.
         * @param headerStart Pointer to the beginning of the header.
         * @param remainingSize Number of bytes in the header buffer.
         */
        Ipv4Header(const uint8_t *headerStart, uint64_t remainingSize);

        /// Get the total size of this packet.
        uint64_t getTotalSize() const;

        /// Print content of this header.
        virtual void printContent() const override final;
    private:
        /// Container for the Ipv4 header.
        const Ipv4HeaderForm *mIpv4Header{nullptr};
    protected:
    };
}

#endif //PCAPANALYSER_IPV4HEADER_H
