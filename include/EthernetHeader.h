/**
 * @file EthernetHeader.h
 * @author Tomas Polasek
 * @brief Class for containing the Ethernet header info.
 */

#ifndef PCAPANALYSER_ETHERNETHEADER_H
#define PCAPANALYSER_ETHERNETHEADER_H

#include "BaseNetworkHeader.h"

namespace pa
{
    class EthernetHeader : public BaseNetworkHeader
    {
    public:
        /// Code for the ipv4 network protocol.
        static constexpr uint16_t NET_TYPE_IPV4{0x0800};
        /// Code for the ipv6 network protocol.
        static constexpr uint16_t NET_TYPE_IPV6{0x86dd};
        /// Code for the arp network protocol.
        static constexpr uint16_t NET_TYPE_ARP{0x0806};
        /// Size of the 802.1Q header in bytes.
        static constexpr uint64_t ETH_DOT1Q_SIZE{4};
        /// Size of the 802.1Q header in uint16's.
        static constexpr uint64_t ETH_DOT1Q_SIZE_UINT16{ETH_DOT1Q_SIZE / 2};
        /// Code for the 802.1Q single tag frame.
        static constexpr uint16_t ETH_DOT1Q_SINGLE{0x8100};
        /// Code for the 802.1Q double tag frame.
        static constexpr uint16_t ETH_DOT1Q_DOUBLE{0x9100};

        /**
         * Analyse the given Ethernet header.
         * @param headerStart Ptr to where the header starts.
         * @param remainingSize Remaining size in bytes.
         */
        EthernetHeader(const uint8_t *headerStart, uint64_t remainingSize);

        /// Print content of this header.
        virtual void printContent() const override final;
    private:
        /// Container for the Ethernet header.
        const EthernetHeaderForm *mEthernetHeader;
    protected:
    };
}

#endif //PCAPANALYSER_ETHERNETHEADER_H
