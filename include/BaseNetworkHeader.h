/**
 * @file BaseNetworkHeader.h
 * @author Tomas Polasek
 * @brief Base class for the in-packet headers.
 */

#ifndef PCAPANALYSER_BASENETWORKHEADER_H
#define PCAPANALYSER_BASENETWORKHEADER_H

#include <cstring>
#include <iostream>

#include "Types.h"
#include "NetAddress.h"

namespace pa
{
    class BaseNetworkHeader
    {
    public:
        /**
         * @param baseSize Base size of the header, this can be changed later.
         * @param layerType Protocol used in this layer.
         */
        BaseNetworkHeader(uint64_t baseSize, FilterT layerType) :
            mSize{baseSize},
            mLayerType{layerType}
        { }

        virtual ~BaseNetworkHeader()
        { }

        /// Get the size of this header in bytes.
        uint64_t headerSize() const
        { return mSize; }

        /// Get the protocol of the next layer.
        FilterT nextLayerType() const
        { return mNextLayerType; }

        /// Get the protocol of this layer.
        FilterT layerType() const
        { return mLayerType; }

        /// Get the source address.
        virtual const NetAddress &src() const
        { return mSrcAddress; }

        /// Get the destination address.
        virtual const NetAddress &dest() const
        { return mDestAddress; }

        /// Print content of this header.
        virtual void printContent() const = 0;
    private:
    protected:
        /// Header size in bytes.
        uint64_t mSize{0};
        /// Protocol of the next layer.
        FilterT mNextLayerType{FilterT::UNK};
        /// Protocol of this layer.
        FilterT mLayerType{FilterT::UNK};
        /// Source address.
        NetAddress mSrcAddress;
        /// Destination address.
        NetAddress mDestAddress;
    };
}

#endif //PCAPANALYSER_BASENETWORKHEADER_H
