/**
 * @file PGH.h
 * @author Tomas Polasek
 * @brief Class used for interpreting binary data of global pcap header.
 */

#ifndef PCAPANALYSER_PGH_H
#define PCAPANALYSER_PGH_H

#include <istream>
#include <iostream>

#include "Types.h"
#include "Util.h"

namespace pa
{
    class PGH
    {
    public:
        /// Normal magic number value.
        static constexpr uint32_t MAGIC_NORMAL{0xa1b2c3d4};
        /// Reversed magic number value.
        static constexpr uint32_t MAGIC_REVERSED{0xd4c3b2a1};
        /// Normal nanosecond magic number value.
        static constexpr uint32_t MAGIC_NANO_NORMAL{0xa1b23c4d};
        /// Reversed nanosecond magic number value.
        static constexpr uint32_t MAGIC_NANO_REVERSED{0x4d3cb2a1};
        /// Ethernet LinkType.
        static constexpr uint32_t ETHERNET_LINKTYPE{1};

        /**
         * Read the data from the input stream and interpret it.
         * @param inputStream Input stream containing the file.
         */
        PGH(std::istream *inputStream);

        /// Print out the header data.
        void printContent() const;

        bool shouldSwap() const
        { return mSwap; }
        bool nanoS() const
        { return mNanoS; }
    private:
        /// Container for the binary data.
        PcapGlobalHeaderForm mHeader;

        /// Is nibble swapping required?
        bool mSwap{false};

        /// Are the times given in nano seconds?
        bool mNanoS{false};
    protected:
    };
}

#endif //PCAPANALYSER_PGH_H
